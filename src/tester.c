#include <sys/mman.h>
#include <stdio.h>

#include "tester.h"
#include "mem.h"
#include "mem_internals.h"
#include "util.h"

void* my_heap;

static struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

bool test_heap_init(){
    my_heap = heap_init(3064);

    if (my_heap){
        fprintf(stdout,"Success\n");
        return true;
    }
    fprintf(stdout,"Failed\n");
    return false;
}

bool test1(){
    void *mall = _malloc(1024);

    if (! mall) {
        fprintf(stdout,"Allocating failed\n");
        return false;
    }
    _free(mall);
    fprintf(stdout,"Allocating is successful\n");

    debug_heap(stdout, my_heap);

    return true;
}

bool test2(){
    void* mall = _malloc(1024);
    void* mall1 = _malloc(1024);

    _free(mall);

    if (!mall && mall1){
        fprintf(stdout, "Freed successfully\n");
        _free(mall1);
        return true;
    }

    _free(mall1);

    debug_heap(stdout,my_heap);

    fprintf(stdout,"Freed failed\n");
    return false;
}

bool test3(){
    void* mall = _malloc(1024);
    void* mall1 = _malloc(1024);
    void* mall2 = _malloc(512);

    _free(mall1);
    _free(mall2);

    if (!mall1 && !mall2 && mall){
        fprintf(stdout,"Freed 2 successfully\n");
        _free(mall);
        return true;
    }

    _free(mall);

    debug_heap(stdout,my_heap);

    fprintf(stdout,"Freed 2 failed\n");
    return false;
}

bool test4(){
    void* mall = _malloc(2560);
    void* mall1 = _malloc(1536);

    struct block_header* header = block_get_header(mall);

    size_t count_memory_blocks = 0;

    while(header){
        header = header->next;
        count_memory_blocks ++;
    }

    if (count_memory_blocks == 2){
        fprintf(stdout,"Memalloc existing successfully\n");
        _free(mall);
        _free(mall1);
        return true;
    }

    fprintf(stdout,"Memalloc existing failed\n");
    _free(mall);
    _free(mall1);

    debug_heap(stdout,my_heap);

    return false;
}

bool test5(){
    void* mall = _malloc(2560);

    struct block_header* header = block_get_header(mall);

    while (header->next){
        header = header->next;
    }

    void* reserve_next_space = (uint8_t*) header + size_from_capacity(header->capacity).bytes;
    reserve_next_space = mmap( reserve_next_space, 1024, PROT_READ | PROT_WRITE, MAP_PRIVATE, 0, 0 );

    void* mall1 = _malloc(2560);

    if (!mall1) {
        _free(mall);
        fprintf(stdout,"Allocating remote memory failed\n");
        return false;
    }

    fprintf(stdout,"Allocating remote memory is successful\n");
    _free(mall);
    _free(mall1);

    debug_heap(stdout,my_heap);

    return true;
}
