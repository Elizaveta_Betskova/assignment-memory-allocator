#ifndef MEMORY_ALLOCATOR_TESTER_H
#define MEMORY_ALLOCATOR_TESTER_H

#include <stdint.h>
#include <stdbool.h>

bool test_heap_init();
bool test1();
bool test2();
bool test3();
bool test4();
bool test5();

#endif //MEMORY_ALLOCATOR_TESTER_H
